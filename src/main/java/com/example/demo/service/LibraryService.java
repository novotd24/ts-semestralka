package com.example.demo.service;

import com.example.demo.model.Book;
import com.example.demo.model.BookCopy;

public interface LibraryService {

    BookCopy addBookToLibrary(Long bookId, Long libraryId);
}
