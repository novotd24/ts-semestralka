package com.example.demo.service;

import com.example.demo.model.Author;
import com.example.demo.model.Book;
import com.example.demo.model.PublisherHouse;

public interface PublisherService {

    PublisherHouse createContractWithAuthor(Long authorId, Long publisherHouseId);
    Book publishBook(Long bookId, Long publisherHouseId);
}
