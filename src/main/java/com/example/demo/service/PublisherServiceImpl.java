package com.example.demo.service;

import com.example.demo.model.Author;
import com.example.demo.model.Book;
import com.example.demo.model.PublisherHouse;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.BookRepository;
import com.example.demo.repository.PublisherHouseRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PublisherServiceImpl implements PublisherService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final PublisherHouseRepository publisherHouseRepository;

    @Autowired
    public PublisherServiceImpl(BookRepository bookRepository, AuthorRepository authorRepository, PublisherHouseRepository publisherHouseRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.publisherHouseRepository = publisherHouseRepository;
    }

    @Override
    public PublisherHouse createContractWithAuthor(Long authorId, Long publisherHouseId) {
        Optional<Author> author = authorRepository.findById(authorId);
        Optional<PublisherHouse> publisherHouse = publisherHouseRepository.findById(publisherHouseId);
        if (author.isEmpty()) {
            throw new EntityNotFoundException("Author not found");
        }
        if (publisherHouse.isEmpty()) {
            throw new EntityNotFoundException("Publisher house not found");
        }
        Author a = author.get();
        PublisherHouse p = publisherHouse.get();
        List<PublisherHouse> publisherHouses = a.getPublisherHouses();
        publisherHouses.add(p);
        p.getContractedAuthors().add(a);
        authorRepository.save(a);
        publisherHouseRepository.save(p);
        return p;
    }

    @Override
    public Book publishBook(Long bookId, Long publisherHouseId) {
        Optional<Book> book = bookRepository.findById(bookId);
        Optional<PublisherHouse> publisherHouse = publisherHouseRepository.findById(publisherHouseId);
        if (book.isEmpty()) {
            throw new EntityNotFoundException("Book not found");
        }
        if (publisherHouse.isEmpty()) {
            throw new EntityNotFoundException("Publisher house not found");
        }
        Book b = book.get();
        PublisherHouse p = publisherHouse.get();
        b.setPublisherHouse(p);
        b.setPublishDate(new Date());
        p.getPublishedBooks().add(b);
        bookRepository.save(b);
        publisherHouseRepository.save(p);
        return b;
    }
}
