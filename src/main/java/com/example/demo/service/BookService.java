package com.example.demo.service;

import com.example.demo.model.Book;

import java.util.Optional;

public interface BookService {
    void createBook(String authorName, String authorEmail, String ISBN, String bookName, String genre);
    Optional<Book> getBook(Long id);

    Book getBookByName(String name);
}

