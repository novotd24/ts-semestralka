package com.example.demo.service;

import com.example.demo.model.Author;
import com.example.demo.model.Book;

import com.example.demo.model.PublisherHouse;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository, AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }

    @Override
    public void createBook(String authorName, String authorEmail, String ISBN, String bookName, String genre) {
        //kontrola validnich udaju
        if (authorName.length() <= 2 || authorName.length() >= 40 || (authorEmail != null && authorEmail.contains(" "))
                || !(ISBN.length() == 10 || ISBN.length() == 13)
                || bookName.length() >= 50 || genre.length() < 5 || genre.length() > 50) {
            return;
        }
        List<Book> books = new ArrayList<>();
        List<PublisherHouse> publisherHouses = new ArrayList<>();
        Author author;
        if (!authorRepository.findBySurname(authorName).isEmpty()) {
            author = authorRepository.findBySurname(authorName).get(0);
        } else {
            author = new Author(authorName, authorEmail, books, publisherHouses);
            authorRepository.save(author);
        }
        List<Author> authors = new ArrayList<>();
        authors.add(author);
        Book book = new Book(ISBN, genre, bookName);
        bookRepository.save(book);
        Logger.getLogger("BookServiceImpl").info("Book created");
    }

    @Override
    public Optional<Book> getBook(Long id) {
        return bookRepository.findById(id);
    }

    @Override
    public Book getBookByName(String name) {
        return bookRepository.findByName(name);
    }

}


