package com.example.demo.service;

import com.example.demo.model.Book;
import com.example.demo.model.BookCopy;
import com.example.demo.model.Library;
import com.example.demo.repository.BookCopyRepository;
import com.example.demo.repository.LibraryRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LibraryServiceImpl implements LibraryService {

    private LibraryRepository libraryRepository;
    private BookCopyRepository bookRepository;

    @Autowired
    public LibraryServiceImpl(LibraryRepository libraryRepository, BookCopyRepository bookRepository) {
        this.libraryRepository = libraryRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public BookCopy addBookToLibrary(Long bookId, Long libraryId) {
        Optional<BookCopy> book = bookRepository.findById(bookId);
        Optional<Library> library = libraryRepository.findById((libraryId));
        if (book.isEmpty()) {
            throw new EntityNotFoundException("Book not found");
        }
        if (library.isEmpty()) {
            throw new EntityNotFoundException("Library not found");
        }
        BookCopy b = book.get();
        Library l = library.get();
        l.getBooks().add(b);
        b.setLibrary(l);
        libraryRepository.save(l);
        bookRepository.save(b);
        return b;
    }
}
