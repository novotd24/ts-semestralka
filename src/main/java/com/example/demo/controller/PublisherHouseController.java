package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.model.PublisherHouse;
import com.example.demo.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/publisherHouse")
public class PublisherHouseController {

    private final PublisherService publisherService;

    @Autowired
    public PublisherHouseController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @PostMapping("/createContract/{publisherHouseId}/authors/{authorId}")
    public ResponseEntity<PublisherHouse> createContract(@PathVariable Long publisherHouseId, @PathVariable Long authorId) {
        PublisherHouse publisherHouse = publisherService.createContractWithAuthor(authorId, publisherHouseId);
        return ResponseEntity.ok().body(publisherHouse);
    }

    @PostMapping("/publishBook/{bookId}/publisherHouse/{publisherHouseId}")
    public ResponseEntity<Book> publishBook(@PathVariable Long bookId, @PathVariable Long publisherHouseId) {
        Book book = publisherService.publishBook(bookId, publisherHouseId);
        return ResponseEntity.ok().body(book);
    }

}
