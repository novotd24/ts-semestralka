package com.example.demo.controller.interceptor;

import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface LoggingStrategy {

    Logger log = LoggerFactory.getLogger(LoggerInterceptor.class);

    void logPreHandle(HttpServletRequest request);

    void logPostHandle(HttpServletRequest request);

}


