package com.example.demo.controller.interceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class LoggerInterceptor implements HandlerInterceptor {

    private final LoggingStrategy strategy;

    @Autowired
    public LoggerInterceptor(ShortLoggingStrategy shortLoggingStrategy, DetailedLoggingStrategy detailedLoggingStrategy) {
        this.strategy = shortLoggingStrategy;
    }

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) throws Exception {

        strategy.logPreHandle(request);

        return true;
    }

    @Override
    public void postHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler,
            ModelAndView modelAndView) throws Exception {

        strategy.logPostHandle(request);
    }
}
