package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/book")
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/create")
    public void createBook(@RequestBody Map<String, String> requestBody) {
        String authorName = requestBody.get("authName");
        String authorEmail = requestBody.get("authEmail");
        String isbn = requestBody.get("ISBN");
        String bookName = requestBody.get("bookName");
        String genre = requestBody.get("genre");

        bookService.createBook(authorName, authorEmail, isbn, bookName, genre);
    }

    @Cacheable("book")
    @GetMapping("/get/{id}")
    public Optional<Book> getBookById(@PathVariable Long id) {
        return bookService.getBook(id);
    }

    @GetMapping("/getByName/{name}")
    public Book getBookByName(@PathVariable String name) {
        return bookService.getBookByName(name);
    }
}
