package com.example.demo.controller;

import com.example.demo.model.BookCopy;
import com.example.demo.model.PublisherHouse;
import com.example.demo.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LibraryController {

    private final LibraryService libraryService;

    @Autowired
    public LibraryController(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    @PostMapping("/addBook/{bookId}/library/{libraryId}")
    public ResponseEntity<BookCopy> addBook(@PathVariable Long bookId, @PathVariable Long libraryId) {
        BookCopy bookCopy = libraryService.addBookToLibrary(bookId, libraryId);
        return ResponseEntity.ok().body(bookCopy);
    }
}
