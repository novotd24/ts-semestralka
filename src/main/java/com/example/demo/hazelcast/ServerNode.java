package com.example.demo.hazelcast;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.flakeidgen.FlakeIdGenerator;

import java.util.Map;

public class ServerNode {

    HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();
    Map<Long, String> map = hazelcastInstance.getMap("data");
    FlakeIdGenerator idGenerator = hazelcastInstance.getFlakeIdGenerator("newid");
}
