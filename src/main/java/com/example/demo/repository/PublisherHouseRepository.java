package com.example.demo.repository;

import com.example.demo.model.PublisherHouse;
import org.springframework.data.repository.CrudRepository;

public interface PublisherHouseRepository extends CrudRepository<PublisherHouse, Long> {
}
