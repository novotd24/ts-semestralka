package com.example.demo.repository;

import com.example.demo.model.BookCopy;
import org.springframework.data.repository.CrudRepository;

public interface BookCopyRepository extends CrudRepository<BookCopy, Long> {
}
