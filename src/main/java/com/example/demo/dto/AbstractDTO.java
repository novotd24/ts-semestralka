package com.example.demo.dto;

public abstract class AbstractDTO implements Cloneable{

    private Long id;
    private String name;

    public AbstractDTO() {
    }

    public AbstractDTO(AbstractDTO dto) {
        id = dto.getId();
        name = dto.getName();
    }

    protected abstract AbstractDTO clone();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
