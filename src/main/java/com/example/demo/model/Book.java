package com.example.demo.model;

//import lombok.Getter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.antlr.v4.runtime.misc.NotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "books")
public class Book extends AbstractEntity {

    @Column(unique = true)
    @NotNull
    private String ISBN;

    @Column
    private String genre;

    @Column
    private String name;

    @Column
    private Date publishDate;

    @ManyToMany
    @JoinTable(
            name = "book_author",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    @JsonIgnore
    private List<Author> authors;

    @ManyToOne
    @JoinColumn(name = "publisher_house_id")
    @JsonIgnore
    private PublisherHouse publisherHouse;

    @OneToMany(mappedBy = "book")
    @JsonIgnore
    private List<BookCopy> bookCopies;

    public Book() {
    }

    public Book( String ISBN, String name, String genre ) {
        this.ISBN = ISBN;
        this.genre = genre;
        this.name = name;
        this.publishDate = new Date();
        this.authors = new ArrayList<>();
        this.bookCopies = new ArrayList<>();
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public PublisherHouse getPublisherHouse() {
        return publisherHouse;
    }

    public void setPublisherHouse(PublisherHouse publisherHouse) {
        this.publisherHouse = publisherHouse;
    }

    public List<BookCopy> getBookCopies() {
        return bookCopies;
    }

    public void setBookCopies(List<BookCopy> bookCopies) {
        this.bookCopies = bookCopies;
    }
}
