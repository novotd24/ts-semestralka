package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.List;

@Entity
public class Author extends AbstractEntity{

    private String surname;
    @NotNull
    private String email;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "authors")
    @JsonIgnore
    private List<Book> books;

    @ManyToMany
    @JsonIgnore
    @JoinTable(
            name = "author_publisher",
            joinColumns = @JoinColumn(name = "author_id"),
            inverseJoinColumns = @JoinColumn(name = "publisher_id"))
    private List<PublisherHouse> publisherHouses;

    public Author() {

    }

    public Author(String surname, String email, List<Book> books, List<PublisherHouse> publisherHouses) {
        this.surname = surname;
        this.email = email;
        this.books = books;
        this.publisherHouses = publisherHouses;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public List<PublisherHouse> getPublisherHouses() {
        return publisherHouses;
    }

    public void setPublisherHouses(List<PublisherHouse> publisherHouses) {
        this.publisherHouses = publisherHouses;
    }
}
