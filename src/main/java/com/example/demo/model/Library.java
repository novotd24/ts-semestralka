package com.example.demo.model;

import jakarta.persistence.*;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.List;

@Entity
public class Library extends AbstractEntity{

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @OneToMany(mappedBy = "library", fetch = FetchType.EAGER)
    private List<BookCopy> books;

    public Library() {
    }

    public Library( Address address, List<BookCopy> books ) {
        this.address = address;
        this.books = books;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<BookCopy> getBooks() {
        return books;
    }

    public void setBooks(List<BookCopy> books) {
        this.books = books;
    }
}
