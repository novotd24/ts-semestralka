package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;

@Entity
public class PublisherHouse extends AbstractEntity{

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @OneToMany(mappedBy = "publisherHouse", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Book> publishedBooks;

    @ManyToMany(mappedBy = "publisherHouses", fetch = FetchType.EAGER)
    private List<Author> contractedAuthors;

    public PublisherHouse( Address address, List<Book> publishedBooks, List<Author> contractedAuthors ) {
        this.address = address;
        this.publishedBooks = publishedBooks;
        this.contractedAuthors = contractedAuthors;
    }

    public PublisherHouse() {

    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Book> getPublishedBooks() {
        return publishedBooks;
    }

    public void setPublishedBooks(List<Book> publishedBooks) {
        this.publishedBooks = publishedBooks;
    }

    public List<Author> getContractedAuthors() {
        return contractedAuthors;
    }

    public void setContractedAuthors(List<Author> contractedAuthors) {
        this.contractedAuthors = contractedAuthors;
    }
}
