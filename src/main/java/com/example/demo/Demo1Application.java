package com.example.demo;

import com.example.demo.model.*;
import com.example.demo.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Demo1Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Demo1Application.class);

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookCopyRepository bookCopyRepository;

    @Autowired
    private LibraryRepository libraryRepository;

    @Autowired
    private PublisherHouseRepository publisherHouseRepository;

    public static void main(String[] args) {
        SpringApplication.run(Demo1Application.class, args);
    }

    @Override
    public void run(String... args) {

        log.info("StartApplication...");

        // setup data
        List<Book> books = new ArrayList<>();
        List<PublisherHouse> publisherHouses = new ArrayList<>();
        Author author1 = new Author("prijmeni1", "mail1@email.com", books, publisherHouses);
        Author author2 = new Author("prijmeni2", "mail2@email.com", books, publisherHouses);
        Author author3 = new Author("prijmeni3", "mail3@email.com", books, publisherHouses);

        Library library1 = new Library(null, new ArrayList<>());
        Library library2 = new Library(null, new ArrayList<>());

        Book book1 = new Book("isbn1", "name1", "genre1");
        Book book2 = new Book("isbn2", "name2", "genre2");
        Book book3 = new Book("isbn3", "name3", "genre1");
        Book book4 = new Book("isbn4", "name4", "genre2");

        book1.getAuthors().add( author1 );
        book2.getAuthors().add( author2 );
        book3.getAuthors().add( author2 );
        book3.getAuthors().add( author3 );
        book4.getAuthors().add( author1 );

        author1.getBooks().add( book1 );
        author1.getBooks().add( book4 );
        author2.getBooks().add( book2 );
        author2.getBooks().add( book3 );
        author3.getBooks().add( book3 );

        BookCopy bookCopy1_1 = new BookCopy( 1L, book1, library1 );
        BookCopy bookCopy1_2 = new BookCopy( 2L, book1, library1 );
        BookCopy bookCopy2_1 = new BookCopy( 3L, book2, library2 );
        BookCopy bookCopy3_1 = new BookCopy( 4L, book3, library2 );

        library1.getBooks().add( bookCopy1_1 );
        library1.getBooks().add( bookCopy1_2 );
        library2.getBooks().add( bookCopy2_1 );
        library2.getBooks().add( bookCopy3_1 );

        PublisherHouse publisherHouse1 = new PublisherHouse(null, new ArrayList<>(), new ArrayList<>());
        PublisherHouse publisherHouse2 = new PublisherHouse(null, new ArrayList<>(), new ArrayList<>());
        publisherHouse1.getPublishedBooks().add( book1 );
        publisherHouse2.getPublishedBooks().add( book2 );
        publisherHouse1.getContractedAuthors().add( author1 );
        publisherHouse2.getContractedAuthors().add( author2 );

        book1.setPublisherHouse( publisherHouse1 );
        book2.setPublisherHouse( publisherHouse2 );
        author1.getPublisherHouses().add( publisherHouse1 );
        author2.getPublisherHouses().add( publisherHouse2 );

        libraryRepository.save( library1 );
        libraryRepository.save( library2 );
        publisherHouseRepository.save( publisherHouse1 );
        publisherHouseRepository.save( publisherHouse2 );
        authorRepository.save( author1 );
        authorRepository.save( author2 );
        authorRepository.save( author3 );
        bookRepository.save( book1 );
        bookRepository.save( book2 );
        bookRepository.save( book3 );
        bookRepository.save( book4 );
        bookCopyRepository.save( bookCopy1_1 );
        bookCopyRepository.save( bookCopy1_2 );
        bookCopyRepository.save( bookCopy2_1 );
        bookCopyRepository.save( bookCopy3_1 );

        log.info( "Data saved" );

    }

}
