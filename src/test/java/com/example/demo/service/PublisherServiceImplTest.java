package com.example.demo.service;

import com.example.demo.model.Address;
import com.example.demo.model.Author;
import com.example.demo.model.Book;
import com.example.demo.model.PublisherHouse;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.BookRepository;
import com.example.demo.repository.PublisherHouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class PublisherServiceImplTest {

    @Mock
    private BookRepository bookRepository;

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private PublisherHouseRepository publisherHouseRepository;

    @InjectMocks
    private PublisherServiceImpl publisherService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateContractWithAuthor() {
        Long authorId = 1L;
        Long publisherHouseId = 1L;
        Author author = new Author("Test Author", "test@example.com", new ArrayList<>(), new ArrayList<>());
        PublisherHouse publisherHouse = new PublisherHouse(new Address(), new ArrayList<>(), new ArrayList<>());
        when(authorRepository.findById(authorId)).thenReturn(Optional.of(author));
        when(publisherHouseRepository.findById(publisherHouseId)).thenReturn(Optional.of(publisherHouse));

        PublisherHouse result = publisherService.createContractWithAuthor(authorId, publisherHouseId);

        assertNotNull(result);
        assertTrue(author.getPublisherHouses().contains(publisherHouse));
        assertTrue(publisherHouse.getContractedAuthors().contains(author));
        verify(authorRepository, times(1)).save(author);
        verify(publisherHouseRepository, times(1)).save(publisherHouse);
    }

    @Test
    public void testPublishBook() {
        Long bookId = 1L;
        Long publisherHouseId = 1L;
        Book book = new Book("isbn", "Test Book", "Test Genre");
        PublisherHouse publisherHouse = new PublisherHouse(new Address(), new ArrayList<>(), new ArrayList<>());
        when(bookRepository.findById(bookId)).thenReturn(Optional.of(book));
        when(publisherHouseRepository.findById(publisherHouseId)).thenReturn(Optional.of(publisherHouse));

        Book result = publisherService.publishBook(bookId, publisherHouseId);

        assertNotNull(result);
        assertEquals(publisherHouse, book.getPublisherHouse());
        assertNotNull(book.getPublishDate());
        assertTrue(publisherHouse.getPublishedBooks().contains(book));
        verify(bookRepository, times(1)).save(book);
        verify(publisherHouseRepository, times(1)).save(publisherHouse);
    }
}
