package com.example.demo.service;

import com.example.demo.model.Author;
import com.example.demo.model.Book;
import com.example.demo.model.PublisherHouse;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BookServiceImplTest {

    @Mock
    private BookRepository bookRepository;

    @Mock
    private AuthorRepository authorRepository;

    @InjectMocks
    private BookServiceImpl bookService;

    @Test
    public void testCreateBook() {
        List<Book> books = new ArrayList<>();
        List<PublisherHouse> publisherHouses = new ArrayList<>();
        Author author = new Author("prijmeni", "mail@email.com", books, publisherHouses);
        when(authorRepository.save(any(Author.class))).thenReturn(author);

        bookService.createBook(author.getSurname(), author.getEmail(), "0123456789", "name", "genre");

        verify(authorRepository, times(1)).save(any(Author.class));
        verify(bookRepository, times(1)).save(any(Book.class));
    }

    @Test
    public void testCreateBookWithExistingAuthor() {
        List<Book> books = new ArrayList<>();
        List<PublisherHouse> publisherHouses = new ArrayList<>();
        Author author = new Author("prijmeni", "mail@email.com", books, publisherHouses);
        List<Author> existingAuthors = new ArrayList<>();
        existingAuthors.add(author);
        when(authorRepository.findBySurname(anyString())).thenReturn(existingAuthors);

        bookService.createBook(author.getSurname(), author.getEmail(), "0123456789", "name", "genre");

        verify(authorRepository, never()).save(any(Author.class));
        verify(bookRepository, times(1)).save(any(Book.class));
    }

    @Test
    public void testGetBookById() {
        Long bookId = 1L;
        Book book = new Book();
        when(bookRepository.findById(bookId)).thenReturn(Optional.of(book));

        Optional<Book> retrievedBook = bookService.getBook(bookId);

        assertEquals(book, retrievedBook.orElse(null));
        verify(bookRepository, times(1)).findById(bookId);
    }

    @ParameterizedTest
    @MethodSource("invalidData")
    public void testCreateBookWithInvalidData(String authorName, String authorEmail, String isbn, String bookName, String genre) {
        bookService.createBook(authorName, authorEmail, isbn, bookName, genre);
        verify(authorRepository, never()).save(any(Author.class));
        verify(bookRepository, never()).save(any(Book.class));
    }

    public static Stream<String[]> invalidData() {
        return Stream.of(
                // Case 1
                new String[]{"aa", "email@example.com", "1234567890123", "Book Name", "Short Genre"},
                // Case 2
                new String[]{"aa", "e m a i l@example.com", "1234567890123!", "Very long book name with exactly 50 characters now", "four"},
                // Case 3
                new String[]{"Very long author name to exceed 40 characters limit", "email@example.com", "123", "Short Book Name", "Very very long genre name with at least 50 characters"},
                // Case 4
                new String[]{"Author name", "e m a i l@example.com", "123456789012312135486", "Very long book name with exactly 50 characters now", "Short Genre"},
                // Case 5
                new String[]{"Very long author name to exceed 40 characters limit", "email@example.com", "1234567890", "Very long book name with exactly 50 characters now", "four"},
                // Case 6
                new String[]{"Very long name with spaces to exceed 40 characters limit", "e m a i l@example.com", "1234567890", "Book name", "Very very long genre name with at least 50 characters"},
                // Case 7
                new String[]{"aa", null, "1234567890", "Very long book name with exactly 50 characters now", "Very very long genre name with at least 50 characters"},
                // Case 8
                new String[]{"Author name", null, "1234567890123", "Short Book Name", "four"}
                );
    }
}