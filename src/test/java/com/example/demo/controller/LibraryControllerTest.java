package com.example.demo.controller;

import com.example.demo.model.BookCopy;
import com.example.demo.model.Library;
import com.example.demo.model.PublisherHouse;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.BookCopyRepository;
import com.example.demo.repository.LibraryRepository;
import com.example.demo.repository.PublisherHouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class LibraryControllerTest {
    @LocalServerPort
    private int port;

    private String baseUrl;

    @Autowired
    private BookCopyRepository bookCopyRepository;

    @BeforeEach
    void setUp() {
        baseUrl = "http://localhost:" + port;
    }

    @Test
    public void testAddBookToLibrary() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<Void> getRequestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<Void> getResponseEntity = restTemplate.exchange(baseUrl + "/addBook/1/library/2", HttpMethod.POST, getRequestEntity, Void.class);

        assertEquals(200, getResponseEntity.getStatusCodeValue());
        Optional<BookCopy> bookCopy = bookCopyRepository.findById(1L);
        assertEquals(2L, bookCopy.get().getLibrary().getId());
    }

}
