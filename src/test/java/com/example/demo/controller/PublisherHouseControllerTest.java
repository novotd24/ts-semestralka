package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.model.PublisherHouse;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.PublisherHouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PublisherHouseControllerTest {
    @LocalServerPort
    private int port;

    private String baseUrl;

    @Autowired
    private PublisherHouseRepository publisherHouseRepository;

    @BeforeEach
    void setUp() {
        baseUrl = "http://localhost:" + port + "/publisherHouse";
    }

    @Test
    public void testCreateContractWithAuthor() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<Void> getRequestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<Void> getResponseEntity = restTemplate.exchange(baseUrl + "/createContract/1/authors/3", HttpMethod.POST, getRequestEntity, Void.class);

        assertEquals(200, getResponseEntity.getStatusCodeValue());
        Optional<PublisherHouse> publisherHouse = publisherHouseRepository.findById(1L);
        assertFalse(publisherHouse.get().getContractedAuthors().stream().filter(a -> a.getId() == 3).collect(Collectors.toSet()).isEmpty());
    }

    @Test
    public void testPublishBook() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<Void> getRequestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<Void> getResponseEntity = restTemplate.exchange(baseUrl + "/publishBook/4/publisherHouse/2", HttpMethod.POST, getRequestEntity, Void.class);

        assertEquals(200, getResponseEntity.getStatusCodeValue());
        Optional<PublisherHouse> publisherHouse = publisherHouseRepository.findById(2L);
        assertFalse(publisherHouse.get().getPublishedBooks().stream().filter(a -> a.getId() == 4).collect(Collectors.toSet()).isEmpty());
    }
}
