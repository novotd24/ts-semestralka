package com.example.demo.controller;

import com.example.demo.model.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookControllerTest {

    @LocalServerPort
    private int port;

    private String baseUrl;

    @BeforeEach
    void setUp() {
        baseUrl = "http://localhost:" + port + "/book";
    }

    @Test
    public void testCreateAndGetBook() {
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("authName", "Test Author");
        requestBody.put("authEmail", "test@example.com");
        requestBody.put("ISBN", "1234567890");
        requestBody.put("bookName", "name5");
        requestBody.put("genre", "Test Genre");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        HttpEntity<Map<String, String>> requestEntity = new HttpEntity<>(requestBody, headers);
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Void> responseEntity = restTemplate.exchange(baseUrl + "/create", HttpMethod.POST, requestEntity, Void.class);

        assertEquals(200, responseEntity.getStatusCodeValue());

        HttpEntity<Void> getRequestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<Book> getResponseEntity = restTemplate.exchange(baseUrl + "/getByName/name1", HttpMethod.GET, getRequestEntity, Book.class);

        assertEquals(200, getResponseEntity.getStatusCodeValue());
        assertNotNull(getResponseEntity.getBody());
        assertEquals("isbn1", getResponseEntity.getBody().getISBN());
        assertEquals("genre1", getResponseEntity.getBody().getGenre());
    }

    @Test
    public void testGetById() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<Void> getRequestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<Book> getResponseEntity = restTemplate.exchange(baseUrl + "/get/1", HttpMethod.GET, getRequestEntity, Book.class);

        assertEquals(200, getResponseEntity.getStatusCodeValue());
        assertNotNull(getResponseEntity.getBody());
        assertEquals("isbn1", getResponseEntity.getBody().getISBN());
        assertEquals("genre1", getResponseEntity.getBody().getGenre());
    }
}
